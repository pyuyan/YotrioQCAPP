import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { BrowserAnimationsModule,NoopAnimationsModule } from '@angular/platform-browser/animations';
import { YotrioQCAPP } from './app.component';
import { PanelMenuModule } from 'primeng/panelmenu';
import { LoginPageModule } from '../pages/login/login.module';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import "reflect-metadata";
import { IQCPageModule } from '../pages/iqc/iqc.module';
import { HomePageModule } from '../pages/home/home.module';

@NgModule({
  declarations: [
    YotrioQCAPP,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(YotrioQCAPP),
    BrowserAnimationsModule,
    NoopAnimationsModule,
    LoginPageModule,
    IQCPageModule,
    PanelMenuModule,
    HomePageModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    YotrioQCAPP,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    BarcodeScanner
  ]
})
export class AppModule {}
