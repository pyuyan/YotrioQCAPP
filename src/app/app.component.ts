import { Component,ViewChild } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NavController } from "ionic-angular/navigation/nav-controller";
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { LoginPage } from '../pages/login/login';
import { ContextData } from './ContextData';
@Component({
  templateUrl: 'app.html'
})
export class YotrioQCAPP {
  @ViewChild('mainNav') nav:NavController;
  @ViewChild('menu') menu:MenuController;

  rootPage:any = LoginPage;

  contextdata:ContextData;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    this.contextdata = ContextData.Create();

    platform.ready().then(() => {

        ContextData.SetNavController(this.nav);
        ContextData.SetFuncMenuController(this.menu);
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

