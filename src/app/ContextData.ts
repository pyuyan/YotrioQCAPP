import { Injectable } from "@angular/core";
import { NavController } from "ionic-angular/navigation/nav-controller";
import { MenuController } from "ionic-angular/components/app/menu-controller";

/*
 * 上下文记录存储
*/
@Injectable()
export class ContextData {

    static funcmenu:MenuController;

    public static SetFuncMenuController(menu:MenuController){
        ContextData.funcmenu = menu;
    }

    public static GetFuncMenuController(){
        return ContextData.funcmenu;
    }

    static navctrl:NavController;

    public static SetNavController(nav:NavController){
        ContextData.navctrl = nav;
    }

    public static GetNavController(){
        return ContextData.navctrl;
    }


    static instance: ContextData;
    public static Create() {
        if (!ContextData.instance){
            ContextData.instance = new ContextData();
            ContextData.instance.functree.push(
                {
                    label: '首页',
                    icon: 'fa fa-fw fa-home',
                    funcid: 'HomePage',
                    command:ContextData.instance.NavToFunc
                },
                {
                    label: '日常操作',
                    icon: '',
                    items: [
                        {
                            label: 'IQC来料检验',
                            icon: 'fa fa-fw fa-arrow-circle-right',
                            funcid: 'IQCPage',
                            command:ContextData.instance.NavToFunc
                        },
                    ]
                },
                {
                    label: '查询',
                    icon: '',
                    items: [
                        {
                            label: '查询报表1', 
                            icon: 'fa fa-fw fa-arrow-circle-right',
                            funcid: 'FunctionPage',
                            command:ContextData.instance.NavToFunc
                        },
                        {
                            label: '查询报表2', 
                            icon: 'fa fa-fw fa-arrow-circle-right',
                            funcid: 'ItemSetPage',
                            command:ContextData.instance.NavToFunc
                        }
                    ]
                },
                {
                    label: '登出',
                    icon: 'fa fa-fw fa-ban',
                    funcid: 'LoginPage',
                    command:ContextData.instance.NavToFunc
                },
            );
        }
        return ContextData.instance;
    }

    functree:Array<any> = new Array<any>();

    NavToFunc(event:any){
        let funcid:any = event.item.funcid;
        ContextData.GetNavController().setRoot(funcid,{});
        ContextData.GetFuncMenuController().close();
    }
}