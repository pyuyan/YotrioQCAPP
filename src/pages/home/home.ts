import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { IQCPage } from '../iqc/iqc';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  OpenEditor(){
    this.navCtrl.setRoot(IQCPage);
  }

  suppliers:any = ['ALL','浙江伊丽特工艺品有限公司','浙江龙威灯饰有限公司'];

  filtedList:Array<any> = [
    {
      ArrivedDate:'2018-07-01',
      RcvItems:[
        {
          Supplier:'浙江龙威灯饰有限公司',
          ItemName:'铝支杆~JD1609/15*14*1.2*730',
          SPECS:'A120160900730',
          UOM1:'支',
          QTY1:'200.00',
          UOM2:'公斤',
          QTY2:'20.00'
        },
        {
          Supplier:'浙江伊丽特工艺品有限公司',
          ItemName:'铝支杆~YJLD-2037/30*25*1.2*955',
          SPECS:'A11D203700955',
          UOM1:'支',
          QTY1:'2976.00',
          UOM2:'公斤',
          QTY2:'1099.00'
        },
        {
          Supplier:'浙江龙威灯饰有限公司',
          ItemName:'铝支杆~YJLD-1165A/28*16*1.3*725',
          SPECS:'A11D1165A0725',
          UOM1:'支',
          QTY1:'3321.00',
          UOM2:'公斤',
          QTY2:'1200.00'
        },
      ],
    },
    {
      ArrivedDate:'2018-07-02',
      RcvItems:[
        {
          Supplier:'浙江龙威灯饰有限公司',
          ItemName:'铝支杆~JD1609/15*14*1.2*730',
          SPECS:'A120160900730',
          UOM1:'支',
          QTY1:'200.00',
          UOM2:'公斤',
          QTY2:'20.00'
        },
        {
          Supplier:'浙江伊丽特工艺品有限公司',
          ItemName:'铝支杆~YJLD-2037/30*25*1.2*955',
          SPECS:'A11D203700955',
          UOM1:'支',
          QTY1:'2976.00',
          UOM2:'公斤',
          QTY2:'1099.00'
        },
        {
          Supplier:'浙江伊丽特工艺品有限公司',
          ItemName:'铝支杆~YJLD-1165A/28*16*1.3*725',
          SPECS:'A11D1165A0725',
          UOM1:'支',
          QTY1:'3321.00',
          UOM2:'公斤',
          QTY2:'1200.00'
        },
      ],
    }
  ];

  todoList:Array<any> = [
    {
      ArrivedDate:'2018-07-01',
      RcvItems:[
        {
          Supplier:'浙江龙威灯饰有限公司',
          ItemName:'铝支杆~JD1609/15*14*1.2*730',
          SPECS:'A120160900730',
          UOM1:'支',
          QTY1:'200.00',
          UOM2:'公斤',
          QTY2:'20.00'
        },
        {
          Supplier:'浙江伊丽特工艺品有限公司',
          ItemName:'铝支杆~YJLD-2037/30*25*1.2*955',
          SPECS:'A11D203700955',
          UOM1:'支',
          QTY1:'2976.00',
          UOM2:'公斤',
          QTY2:'1099.00'
        },
        {
          Supplier:'浙江龙威灯饰有限公司',
          ItemName:'铝支杆~YJLD-1165A/28*16*1.3*725',
          SPECS:'A11D1165A0725',
          UOM1:'支',
          QTY1:'3321.00',
          UOM2:'公斤',
          QTY2:'1200.00'
        },
      ],
    },
    {
      ArrivedDate:'2018-07-02',
      RcvItems:[
        {
          Supplier:'浙江龙威灯饰有限公司',
          ItemName:'铝支杆~JD1609/15*14*1.2*730',
          SPECS:'A120160900730',
          UOM1:'支',
          QTY1:'200.00',
          UOM2:'公斤',
          QTY2:'20.00'
        },
        {
          Supplier:'浙江伊丽特工艺品有限公司',
          ItemName:'铝支杆~YJLD-2037/30*25*1.2*955',
          SPECS:'A11D203700955',
          UOM1:'支',
          QTY1:'2976.00',
          UOM2:'公斤',
          QTY2:'1099.00'
        },
        {
          Supplier:'浙江伊丽特工艺品有限公司',
          ItemName:'铝支杆~YJLD-1165A/28*16*1.3*725',
          SPECS:'A11D1165A0725',
          UOM1:'支',
          QTY1:'3321.00',
          UOM2:'公斤',
          QTY2:'1200.00'
        },
      ],
    }
  ];

}
