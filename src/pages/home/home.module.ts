import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ButtonModule } from 'primeng/primeng';
import {InputTextModule} from 'primeng/inputtext';
import {CardModule} from 'primeng/card';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {EditorModule} from 'primeng/editor';
import {DataViewModule} from 'primeng/dataview';
import { TableModule } from 'primeng/table';
import {StepsModule} from 'primeng/steps';
import {ProgressBarModule} from 'primeng/progressbar';
import {DropdownModule} from 'primeng/dropdown';
import {InputSwitchModule} from 'primeng/inputswitch';
import { HomePage } from './home';

@NgModule({
  declarations: [
    HomePage,
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
    ButtonModule,
    InputTextModule,
    CardModule,
    InputTextareaModule,
    EditorModule,
    DataViewModule,
    TableModule,
    StepsModule,
    ProgressBarModule,
    DropdownModule,
    InputSwitchModule,
  ],
})
export class HomePageModule {}
