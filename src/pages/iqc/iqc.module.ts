import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ButtonModule } from 'primeng/primeng';
import {InputTextModule} from 'primeng/inputtext';
import {CardModule} from 'primeng/card';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {EditorModule} from 'primeng/editor';
import {DataViewModule} from 'primeng/dataview';
import { TableModule } from 'primeng/table';
import {StepsModule} from 'primeng/steps';
import {ProgressBarModule} from 'primeng/progressbar';
import { ResultPage } from './result';
import {DropdownModule} from 'primeng/dropdown';
import { EditPage } from './edit';
import {InputSwitchModule} from 'primeng/inputswitch';
import { IQCPage } from './iqc';

@NgModule({
  declarations: [
    IQCPage,
    ResultPage,
    EditPage
  ],
  imports: [
    IonicPageModule.forChild(IQCPage),
    TableModule,
    ButtonModule,
    InputTextModule,
    CardModule,
    InputTextareaModule,
    EditorModule,
    DataViewModule,
    StepsModule,
    ProgressBarModule,
    DropdownModule,
    InputSwitchModule
  ],
  entryComponents:[
    IQCPage,
    ResultPage,
    EditPage
  ],
  exports:[
    IQCPage,
    ResultPage,
    EditPage
  ]
})
export class IQCPageModule {}
