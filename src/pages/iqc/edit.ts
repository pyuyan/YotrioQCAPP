import { Component, ViewChild } from '@angular/core';
import { NavController, Slides, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';

@Component({
    selector: 'page-iqc',
    templateUrl: 'edit.html'
  })
  export class EditPage {

    constructor(public navCtrl: NavController,
      public viewCtrl:ViewController,
      public params:NavParams) {
        this.stepdata = params.get('stepdata');
    }


    GoBack(){
      this.viewCtrl.dismiss();
    }

    headers:any = [
      {name:'样品序号',field:'rowno',width:'20%',editable:false},
      {name:'检验结果',field:'description',width:'60%',editable:false},
      {name:'结论',field:'conclusion',width:'20%',editable:false},
    ];

    conclusions:any = [
      {name:'合格',evalue:0},
      {name:'不合格',evalue:1},
    ];

    stepdata:any = {
      stepname:'',
      description:'',
      toolset:"",
      qclevel:"",
      qctype:'',
      checkreport:{
        detailmode:false,
        details:[

        ],
        re:0,
        remark:''
      }
    };

  }