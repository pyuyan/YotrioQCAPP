import { Component, ViewChild } from '@angular/core';
import { NavController, Slides, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';

@Component({
    selector: 'page-iqc',
    templateUrl: 'result.html'
  })
  export class ResultPage {

    constructor(public navCtrl: NavController,
      public viewCtrl:ViewController,
      public params:NavParams) {
        this.qcresult = params.get('qcresult');

        console.log(params);
        console.log(this.qcresult);
    }


    GoBack(){
      this.viewCtrl.dismiss();
    }

    headers:any = [
      {name:'检查项目',field:'stepname',width:'20%'},
      {name:'检验结果',field:'remark',width:'30%'},
      {name:'A',field:'A',width:'10%'},
      {name:'B',field:'B',width:'10%'},
      {name:'C',field:'C',width:'10%'}
    ];

    qcresult:any = {
      aql:'0/1.0/2.5',
      result:'',
      conclusion:'',
      dealing:{},
      qcobjs:[],
    };

    
  dealings:any = [
    {name:'接受',evalue:0},
    {name:'接受偏差',evalue:1},
    {name:'拣选或返工',evalue:2},
    {name:'退货',evalue:3},
  ];


  }