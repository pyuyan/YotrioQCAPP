import { Component, ViewChild } from '@angular/core';
import { NavController,Slides, ModalController, Modal } from 'ionic-angular';
import { IonicPage } from 'ionic-angular/navigation/ionic-page';
import { ResultPage } from './result';
import { EditPage } from './edit';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

@IonicPage()
@Component({
  selector: 'page-iqc',
  templateUrl: 'iqc.html'
})
export class IQCPage {

  @ViewChild('qcslide') qcslide:Slides;
  
  constructor(public navCtrl: NavController,public modalCtrl:ModalController,
    private barcodeScanner: BarcodeScanner) {

  }
  
  qcprogress:number = 0;

  OpenCamScanner(){
    this.barcodeScanner.scan().then(barcodeData => {
      console.log('Barcode data', barcodeData);
     }).catch(err => {
         console.log('Error', err);
     });
  }

  OpenResult(){
    let reportdata = {
      aql:'0/1.0/2.5',
      result:'',
      conclusion:'不合格',
      dealing:{},
    };
    let qcobjs:Array<any> = new Array<any>();
    let a:number = 0;
    let b:number = 0;
    let c:number = 0;
    this.qcsteps.forEach(qcstep=>{
      let qcobj = {
        stepname:qcstep.stepname,
        remark:qcstep.checkreport.remark,
        A:0,
        B:0,
        C:0
      };
      if(qcstep.qctype==='A'){
        qcobj.A = qcstep.checkreport.re;
        a++;
      }else if(qcstep.qctype==='B'){
        qcobj.B = qcstep.checkreport.re;
        b++;
      }else if(qcstep.qctype==='C'){
        qcobj.C = qcstep.checkreport.re;
        c++;
      }

      reportdata['qcobjs'] = qcobjs;
      reportdata.result = a.toString()+'/'+b.toString()+'/'+c.toString();
      reportdata.dealing = {name:'接受偏差',evalue:1};

      qcobjs.push(qcobj);
    });



    let modal:Modal = this.modalCtrl.create(ResultPage,{qcresult:reportdata});
    modal.present();
  }

  InputQCResult(stepdata:any){
    if((!stepdata.checkreport.details)||stepdata.checkreport.details.length==0)
    {
      for(let i:number=0;i<12;i++){
        stepdata.checkreport.details.push({
          rowno:i+1,
          description:'',
          conclusion:{},
        });
      }
    }

    let modal:Modal = this.modalCtrl.create(EditPage,{stepdata:stepdata});
    modal.present();

    console.log(stepdata);
  }

  slideChanged(){
    let steplength = Math.ceil(100/this.qcsteps.length);
    if(this.qcprogress+steplength>100){
      this.qcprogress = 100;
    }else{
      this.qcprogress+=steplength;
    }
  }

  qcsteps:Array<any> = [
    {
      stepname:'铝材外观',
      description:"① 铝材表面应整洁，不允许有影响喷塑的裂纹、起皮、腐蚀、压坑、碰伤、擦伤等不良，以及模具挤压痕；\r\n"
      +"② 管材有加强筋的，加强筋应无破裂；\r\n"
      +"③ 管材不允许弯曲、扭曲。",
      toolset:"目视/水平面",
      qclevel:"一般/II",
      qctype:'B',
      checkreport:{
        detailmode:false,
        details:[],
        re:1,
        remark:'检验结果说明'
      }
    },
    {
      stepname:'管材端头',
      description:"管材端头不允许切斜；也不允许因锯切产生的变形",
      toolset:"目视/水平面",
      qclevel:"S-1",
      qctype:'C',
      checkreport:{
        detailmode:false,
        details:[],
        re:1,
        remark:'检验结果说明'
      }
    },
    {
      stepname:'横截面/长与宽',
      description:"横截面形状和尺寸应符合研发图纸要求，允许公差±0.1mm",
      toolset:"卡尺",
      qclevel:"一般/II",
      qctype:'B',
      checkreport:{
        detailmode:false,
        details:[],
        re:1,
        remark:'检验结果说明'
      }
    },
    {
      stepname:'横截面/长与宽内径',
      description:"符合研发图纸要求",
      toolset:"卡尺",
      qclevel:"一般/II",
      qctype:'B',
      checkreport:{
        detailmode:false,
        details:[],
        re:1,
        remark:'检验结果说明'
      }
    },
    {
      stepname:'长度',
      description:"符合采购订单要求，允许公差±1mm",
      toolset:"卷尺",
      qclevel:"一般/II",
      qctype:'C',
      checkreport:{
        detailmode:false,
        details:[],
        re:1,
        remark:'检验结果说明'
      }
    },
    {
      stepname:'壁厚',
      description:"铝材厚度允许公差为[-0.11,+0.02]mm。\r\n"
      +"壁厚应均匀，无偏芯现象，轻微偏芯的可经车间试用，不影响产品质量的可接收。\r\n"
      +"铝材按重量结算，偏厚会导致支数的减少、成本增加",
      toolset:"千分尺",
      qclevel:"一般/II",
      qctype:'A',
      checkreport:{
        detailmode:false,
        details:[],
        re:1,
        remark:'检验结果说明'
      }
    },
    {
      stepname:'硬度',
      description:"①直料为11-13HW；\r\n"
      +"②穿布管(打弯料)为8-10HW或按零件表要求；\r\n"
      +"③未时效管为0-6 HW",
      toolset:"韦氏硬度计",
      qclevel:"S-1",
      qctype:'B',
      checkreport:{
        detailmode:false,
        details:[],
        re:1,
        remark:'检验结果说明'
      }
    },
  ];


  activeIndex: number = 0;


}
